import socket

with open("a.txt", "r") as file:
    file_contents = file.read()

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind(("", 2004))

server.listen(1)

print("Server is Connect... ")

client_socket, client_address = server.accept()

print("Accepted connect from:{}".format(client_address))

client_socket.sendall(file_contents.encode())

client_socket.close()
server.close()
