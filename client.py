import socket

target_host = "192.168.90.177"
target_port = 2004

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((target_host, target_port))

file_contents = b"" #แปลงเป็น bytes
data = client.recv(1024)
while data:
    file_contents += data
    data = client.recv(1024)

client.close()

print("Recived file contents: ")
print(file_contents.decode())
